# $Id$
# Maintainer: David Pinheiro <david.pinheiro@gorilux.org>


highmem=1

pkgname=qt5-rpi2
pkgver=5.7.1
pkgrel=1

provides=('qt5-3d'
         'qt5-base'
         'qt5-canvas3d'
         'qt5-connectivity'
         'qt5-declarative'
         'qt5-doc'
         'qt5-enginio'
         'qt5-graphicaleffects'
         'qt5-imageformats'
         'qt5-location'
         'qt5-multimedia'
         'qt5-quick1'
         'qt5-quickcontrols'
         'qt5-script'
         'qt5-sensors'
         'qt5-serialport'
         'qt5-svg'
         'qt5-tools'
         'qt5-translations'
         'qt5-wayland'
         'qt5-webchannel'
         'qt5-webkit'
         'qt5-websockets'
         'qt5-x11extras'
         'qt5-xmlpatterns')
arch=('armv7h')
url='http://www.qt.io/developers'
license=('GPL3' 'LGPL' 'FDL' 'custom')
makedepends=('at-spi2-core' 'alsa-lib' 'gst-plugins-base-libs' 'libjpeg-turbo'
            'libpulse' 'hicolor-icon-theme' 'desktop-file-utils' 'nss'
            'sqlite' 'libmng' 'python2' 'ruby'
            'gperf' 'libxslt' 'fontconfig' 'bluez-libs' 'openal'
            'mtdev' 'harfbuzz' 'libwebp' 'leveldb' 'geoclue' 'pciutils'
            'libinput' 'git' 'icu')
pkgdesc='A cross-platform application and UI framework'
depends=('libjpeg-turbo' 'dbus' 'fontconfig' 'systemd'
           'libxi' 'sqlite' 'icu'
           'libinput' 'libsm')
conflicts=('qt' 'qt5')
groups=('qt' 'qt5')
#_repodir="${pkgname}"
#source=("$_repodir::git+http://code.qt.io/qt/qt5.git#branch=5.6")
source=('0001-RPI2-includepaths.patch'
	'0001-RPI2-compile-fix.patch'
        '0001-Added-eglfs-videooverlay.patch')
md5sums=('SKIP' 
	 'SKIP'
         'SKIP')

#pkgver() {
#    if [ -d "$srcdir/$pkgname" ]; then
#     cd "$srcdir/$pkgname"
#     # cutting off 'foo-' prefix that presents in the git tag
#    git describe --long --tags | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g'
#    fi
#}

prepare() {

  if [ -d "$srcdir/$pkgname"/.git ]; then
     cd "$srcdir/$pkgname"
     git pull
     git reset --hard
     git submodule foreach git reset --hard
  else
    git clone -b 5.7 --depth=1 --single-branch  http://code.qt.io/qt/qt5.git "$srcdir/$pkgname"
    cd "$srcdir/$pkgname"
    git submodule update --init
  fi

  
  #git clone -b 5.6 --depth=1 --single-branch  http://code.qt.io/qt/qt5.git "$srcdir/$pkgname"

  #cd "$srcdir/$pkgname"

  #git submodule init 
  #git submodule update --init

  # ./init-repository -f  

  # Build qmake using Arch {C,LD}FLAGS
  # This also sets default {C,CXX,LD}FLAGS for projects built using qmake
  sed -i -e "s|^\(QMAKE_CFLAGS_RELEASE.*\)|\1 ${CFLAGS}|" \
    qtbase/mkspecs/common/gcc-base.conf
  sed -i -e "s|^\(QMAKE_LFLAGS_RELEASE.*\)|\1 ${LDFLAGS}|" \
    qtbase/mkspecs/common/g++-unix.conf

  patch -p1 -d qtbase -i "${srcdir}"/0001-RPI2-includepaths.patch
  patch -p1 -d qtmultimedia -i "${srcdir}"/0001-Added-eglfs-videooverlay.patch
  patch -p1 -d qtwayland -i "${srcdir}"/0001-RPI2-compile-fix.patch

  MAKEFLAGS="-j9"


  # Use python2 for Python 2.x
  find . -name '*.py' -exec sed -i \
    's|#![ ]*/usr/bin/python$|&2|;s|#![ ]*/usr/bin/env python$|&2|' {} +
  # in qtwebengine there are still a lot of relative calls which need a workaround
  mkdir -p "${srcdir}"/python2-path
  ln -fs /usr/bin/python2 "${srcdir}"/python2-path/python
}

build() {
  cd ${pkgname}
  
  export QTDIR="${srcdir}"/${pkgname}
  export LD_LIBRARY_PATH="${QTDIR}"/qtbase/lib:"${QTDIR}"/qttools/lib:"${LD_LIBRARY_PATH}"
  export QT_PLUGIN_PATH="${QTDIR}"/qtbase/plugins

  # python2 workaround
  export PATH="${srcdir}/python2-path:$PATH"

  export CC="distcc gcc" 
  export CXX="distcc g++" 

  PYTHON=/usr/bin/python2 ./configure -v -confirm-license -opensource \
    -prefix /usr \
    -bindir /usr/lib/qt/bin \
    -docdir /usr/share/doc/qt \
    -headerdir /usr/include/qt \
    -archdatadir /usr/lib/qt \
    -datadir /usr/share/qt \
    -sysconfdir /etc/xdg \
    -examplesdir /usr/share/doc/qt/examples \
    -opengl es2 \
    -device linux-rasp-pi2-g++ \
    -device-option CROSS_COMPILE=/usr/bin/ \
    -sysroot / \
    -no-gcc-sysroot \
    -optimized-qmake \
    -reduce-exports \
    -no-xcb \
    -no-xkbcommon \
    -no-kms \
    -eglfs \
    -qpa eglfs \
    -release \
    -qt-pcre \
    -gstreamer 1.0 \
    -system-sqlite \
    -openssl-linked \
    -no-pch \
    -no-rpath \
    -dbus-linked \
    -system-harfbuzz \
    -journald \
    -libinput \
    -no-use-gold-linker \
    -no-reduce-relocations \
    -no-sse2 \
    -no-sse3 \
    -no-ssse3 \
    -no-sse4.1 \
    -no-sse4.2 \
    -nomake examples \
    -nomake tests \
    -skip qtwebkit \
    -skip qtwebkit-examples \
    -skip qtwebengine


  make

  # Fix docs build when qt is not installed
  #sed -i "s|/usr/lib/qt/bin/qdoc|${QTDIR}/qtbase/bin/qdoc|g" \
  #  "${QTDIR}"/qtbase/qmake/Makefile.qmake-docs
  #find "${QTDIR}" -name Makefile \
  #  -exec sed -i "s|/usr/lib/qt/bin/qdoc|${QTDIR}/qtbase/bin/qdoc|g" {} +
  #sed -i "s|/usr/lib/qt/bin/qhelpgenerator|${QTDIR}/qttools/bin/qhelpgenerator|g" \
  #  "${QTDIR}"/qtbase/qmake/Makefile.qmake-docs
  #find "${QTDIR}" -name Makefile \
  #  -exec sed -i "s|/usr/lib/qt/bin/qhelpgenerator|${QTDIR}/qttools/bin/qhelpgenerator|g" {} +
  #sed -i "s|/usr/lib/qt/bin/qhelpgenerator|${QTDIR}/qttools/bin/qhelpgenerator|g" qtwebkit/Source/Makefile.api
  #find "${QTDIR}" -name Makefile \
  #  -exec sed -i "s|/usr/lib/qt/bin/qmlplugindump|${QTDIR}/qtdeclarative/bin/qmlplugindump|g" {} +

  #make docs
}


package() {
 

  cd ${pkgname}
  make INSTALL_ROOT="${pkgdir}" install

  install -D -m644 LGPL_EXCEPTION.txt \
    "${pkgdir}"/usr/share/licenses/${pkgname}/LGPL_EXCEPTION.txt

  # Drop QMAKE_PRL_BUILD_DIR because reference the build dir
  find "${pkgdir}/usr/lib" -type f -name '*.prl' \
    -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

  # Fix wrong qmake path in pri file
  sed -i "s|${srcdir}/${pkgname}/qtbase|/usr|" \
    "${pkgdir}"/usr/lib/qt/mkspecs/modules/qt_lib_bootstrap_private.pri

  # Useful symlinks
  install -d "${pkgdir}"/usr/bin
  for b in "${pkgdir}"/usr/lib/qt/bin/*; do
    ln -s /usr/lib/qt/bin/$(basename $b) "${pkgdir}"/usr/bin/$(basename $b)
  done
}


# qt5-rpi2 package for archlinuxarm #

Here you can find the PKGBUILD and the necessary patches to create fully functional EGL qt5 compiled for RPI2.

That's nice, but I don't have any time to compile the source. Where can I download a precompiled version?  

You can add the following entries to your pacman.conf:

```
[grlx]
Server = https://www.gorilux.org/repo/armv7h
SigLevel = Optional TrustAll

```